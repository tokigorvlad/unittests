﻿using HrDepartment.Application.Interfaces;
using HrDepartment.Application.Services;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrDepartment.ApplicationTests
{
    class JobSeekerRateServiceTests
    {

        Mock<ISanctionService> _sanctionServiceTrue;
        Mock<ISanctionService> _sanctionServiceFalse;
        Mock<INotificationService> _sanctionServiceNull;
        private ISanctionService SanctionServiceTrue => _sanctionServiceTrue.Object;
        private ISanctionService SanctionServiceFalse => _sanctionServiceFalse.Object;
        private ISanctionService SanctionServiceNull => _sanctionServiceNull.Object as ISanctionService;
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _sanctionServiceTrue = new Mock<ISanctionService>();
            _sanctionServiceTrue.Setup(c => c.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>())).ReturnsAsync(true);
            _sanctionServiceFalse = new Mock<ISanctionService>();
            _sanctionServiceFalse.Setup(c => c.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>())).ReturnsAsync(false);
            _sanctionServiceNull = new Mock<INotificationService>();
            _sanctionServiceNull.Setup(c => c.NotifyRockStarFoundAsync(It.IsAny<JobSeeker>()));
          
        }
        [SetUp]
        public void Setup()
        {
           
        }
        [Test]
        public void CalculateJobSeekerRatingAsync_SanctionServiceisNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            { GetJobSeekerRateService(SanctionServiceNull); });
            
        }
        [Test]
        public void CalculateJobSeekerRatingAsync_SanctionServiceisTrue_ShouldReturnCorrectResult()
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceTrue);
            var result = jobSeekerRateService.CalculateJobSeekerRatingAsync(new JobSeeker());
            var expectedResult = 0;
            Assert.AreEqual(expectedResult, result.Result);
        }
        [Test]
        public void CalculateJobSeekerRatingAsync_SanctionServiceisFalse_ShouldReturnCorrectResult()
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
            var result = jobSeekerRateService.CalculateJobSeekerRatingAsync(new JobSeeker() );
            var expectedResult = 15;
            Assert.AreEqual(expectedResult, result.Result);
        }
        
        [TestCase(16, 15)]
        [TestCase(30, 25)]
        [TestCase(70, 15)]
        public void CalculateJobSeekerRatingAsync_Age_ShouldReturnCorrectResult(int age, int expectedResult)
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
            var result = jobSeekerRateService.CalculateJobSeekerRatingAsync(new JobSeeker() {BirthDate = DateTime.Now.AddYears(-age) });
            Assert.AreEqual(expectedResult, result.Result);
        }
        [TestCase(EducationLevel.None, 15)]
        [TestCase(EducationLevel.School, 20)]
        [TestCase(EducationLevel.College, 30)]
        [TestCase(EducationLevel.University, 50)]
        public void CalculateJobSeekerRatingAsync_Education_ShouldReturnCorrectResult(EducationLevel educationLevel, int expectedResult)
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
            var result = jobSeekerRateService.CalculateJobSeekerRatingAsync(new JobSeeker() { Education = educationLevel });
            Assert.AreEqual(expectedResult, result.Result);
        }
        [Test]
        public void CalculateJobSeekerRatingAsync_EducationIsNotExist_ThrowsArgumentOutOfRangeException()
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
             Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => 
             { await jobSeekerRateService.CalculateJobSeekerRatingAsync(new JobSeeker() { Education = (EducationLevel)(-1) }); });
        }
        
        [TestCase(int.MaxValue)]
        [TestCase(int.MinValue)]
        public void CalculateJobSeekerRatingAsync_EducationIsNull_ThrowsArgumentOutOfRangeException(int level)
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
            Assert.Throws<ArgumentOutOfRangeException>(() =>
            { jobSeekerRateService.CalculateJobSeekerRatingAsync(new JobSeeker() {Education=(EducationLevel)(level) }).GetAwaiter().GetResult(); });
        }
        [TestCase(0, 15)]
        [TestCase(2, 20)]
        [TestCase(4, 25)]
        [TestCase(9, 35)]
        [TestCase(20, 50)]
        public void CalculateJobSeekerRatingAsync_Experience_ShouldReturnCorrectResult(int experience, int expectedResult)
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
            var result = jobSeekerRateService.CalculateJobSeekerRatingAsync(new JobSeeker() { Experience = experience });
            Assert.AreEqual(expectedResult, result.Result);
        }
        [TestCase(BadHabits.None, 15)]
        [TestCase(BadHabits.Smoking, 10)]
        [TestCase(BadHabits.Alcoholism, 0)]
        [TestCase(BadHabits.Drugs, 0)]
        public void CalculateJobSeekerRatingAsync_Habbits_ShouldReturnCorrectResult(BadHabits badHabits, int expectedResult)
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
            var result = jobSeekerRateService.CalculateJobSeekerRatingAsync(new JobSeeker() { BadHabits = badHabits });
            Assert.AreEqual(expectedResult, result.Result);
        }
        
        [Test]
        public void CalculateJobSeekerRatingAsync_TotalRateisNegative_ShouldReturnCorrectResult()
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
            JobSeeker jobSeekerRaitingByNegative =
            new JobSeeker
            {
                BirthDate = DateTime.Now.AddYears(-16),
                BadHabits = BadHabits.Drugs,
                Education = EducationLevel.None,
                Experience = 0
            };
            var result = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeekerRaitingByNegative);
            int expectedResult = 0;
            Assert.AreEqual(expectedResult, result.Result);
        }
        [Test]
        public void CalculateJobSeekerRatingAsync_TotalRateisMore100_ShouldReturnCorrectResult()
        {
            var jobSeekerRateService = GetJobSeekerRateService(SanctionServiceFalse);
            JobSeeker jobSeekerRaitingByMore100 =
            new JobSeeker
            {
                BirthDate = DateTime.Now.AddYears(-60),
                BadHabits = BadHabits.None,
                Education = EducationLevel.University,
                Experience = 40
            };
            var result = jobSeekerRateService.CalculateJobSeekerRatingAsync(jobSeekerRaitingByMore100);
            int expectedResult = 95;
            Assert.AreEqual(expectedResult, result.Result);
        }
        private JobSeekerRateService GetJobSeekerRateService(ISanctionService sanctionService)
        {
            return new JobSeekerRateService(sanctionService);
        }

    }
}
