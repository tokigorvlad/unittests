using NUnit.Framework;
using HrDepartment.Application.Interfaces;
using HrDepartment.Infrastructure.Interfaces;
using AutoMapper;
using HrDepartment.Domain.Entities;
using Moq;
using HrDepartment.Application.Services;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HrDepartment.ApplicationTests
{
    public class JobSeekerServiceTests
    {
        Mock<IStorage> _storageMock;
        private IStorage Storage => _storageMock.Object;
        Mock<IRateService<JobSeeker>> _rateServiceMockReturn0;
        private IRateService<JobSeeker> RateService0 => _rateServiceMockReturn0.Object;

        Mock<IRateService<JobSeeker>> _rateServiceMockReturn100;
        private IRateService<JobSeeker> RateService100 => _rateServiceMockReturn100.Object;
        Mock<INotificationService> _notificationServiceMock;
        private INotificationService NotificationService => _notificationServiceMock.Object;
        Mock<IMapper> _mapperMock;
        private IMapper Mapper => _mapperMock.Object;
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            JobSeeker jobSeeker = new JobSeeker() { Id = 0 };
            int ratingmin = 0;
            int ratingmax = 100;
            string taskResult = "Complete";
            _storageMock = new Mock<IStorage>();
            _storageMock.Setup(c => c.AddAsync<JobSeeker>(jobSeeker)).Returns(Task.FromResult(jobSeeker));
            _storageMock.Setup(c => c.DeleteAsync(jobSeeker)).Returns(Task.CompletedTask);
            _storageMock.Setup(c => c.UpdateAsync<JobSeeker>(jobSeeker)).Returns(Task.FromResult(jobSeeker));
            _storageMock.Setup(c => c.GetAllAsync<JobSeeker>()).ReturnsAsync(new List<JobSeeker>() { new JobSeeker {} });
            _storageMock.Setup(c => c.GetByIdAsync<JobSeeker,int>(jobSeeker.Id)).ReturnsAsync(jobSeeker);

            _rateServiceMockReturn0 = new Mock<IRateService<JobSeeker>>();
            _rateServiceMockReturn0.Setup(c => c.CalculateJobSeekerRatingAsync(jobSeeker)).Returns(Task.FromResult(ratingmin));

            _rateServiceMockReturn100 = new Mock<IRateService<JobSeeker>>();
            _rateServiceMockReturn100.Setup(c => c.CalculateJobSeekerRatingAsync(jobSeeker)).Returns(Task.FromResult(ratingmax));

            _notificationServiceMock = new Mock<INotificationService>();
            _notificationServiceMock.Setup(c => c.NotifyRockStarFoundAsync(jobSeeker)).Returns(Task.FromResult(taskResult));

            _mapperMock = new Mock<IMapper>();
        }

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void RateJobSeekerAsync_Rating0_()
        {
            var jobSeekerService = GetJobSeekerService(RateService0);
            var result =jobSeekerService.RateJobSeekerAsync(0);
            var expectedResult = 0;
            Assert.AreEqual(expectedResult, result.Result);
        }
        [Test]
        public void RateJobSeekerAsync_Ratingis100()
        {

            var jobSeekerService = GetJobSeekerService(RateService100);
            
            var result = jobSeekerService.RateJobSeekerAsync(0);
            var expectedResult = 100;
            Assert.AreEqual(expectedResult, result.Result);
        }
        [Test]
        public void RateJobSeekerAsync_Ratingis100Verify_()
        {

            var jobSeekerService = GetJobSeekerService(RateService100);

            var result = jobSeekerService.RateJobSeekerAsync(0);
            _notificationServiceMock.Verify(x => x.NotifyRockStarFoundAsync(It.IsAny<JobSeeker>()));
           
        }
        private JobSeekerService GetJobSeekerService(IRateService<JobSeeker> rateService)
        {
            return new JobSeekerService(Storage, rateService, Mapper,NotificationService);
        }
    }
}